<?php 
/**
 *用户管理
*/

namespace xhadmin\service\admin;

use xhadmin\CommonService;
use xhadmin\db\User;

class UserService extends CommonService {


	/*
 	* @Description  用户管理  <font color="red">初始密码都为000000</font>列表数据
 	* @param (输入参数：)  {array}        where 查询条件
 	* @param (输入参数：)  {int}          limit 分页参数
 	* @param (输入参数：)  {String}       field 查询字段
 	* @param (输入参数：)  {String}       orderby 排序字段
 	* @return (返回参数：) {array}        分页数据集
 	*/
	public static function pageList($where=[],$limit,$field='*',$orderby=''){
		try{
			$list = User::loadList($where,$limit,$field,$orderby);
			$count = User::countList($where);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return ['list'=>$list,'count'=>$count];
	}


	/*
 	* @Description  添加账户
 	* @param (输入参数：)  {array}        data 原始数据
 	* @return (返回参数：) {bool}        
 	*/
	public static function add($data){
		try{
			$data['create_time'] = time();

			//数据验证
			$rule = [
				'name'=>['require'],
				'user'=>['require'],
				'pwd'=>['require','regex'=>'/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/'],
			];
			//数据错误提示
			$msg = [
				'name.require'=>'真实姓名不能为空',
				'user.require'=>'用户名不能为空',
				'pwd.require'=>'密码不能为空',
				'pwd.regex'=>'6-21位数字',
			];
			self::validate($rule,$data,$msg);

			$data['pwd'] = md5($data['pwd'].config('my.password_secrect'));
			$res = User::createData($data);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}


	/*
 	* @Description  修改账户
 	* @param (输入参数：)  {array}        data 原始数据
 	* @return (返回参数：) {bool}        
 	*/
	public static function update($data){
		try{
			$data['create_time'] = strtotime($data['create_time']);

			//数据验证
			$rule = [
				'name'=>['require'],
				'user'=>['require'],
			];
			$msg = [
				'name.require'=>'真实姓名不能为空',
				'user.require'=>'用户名不能为空',
			];
			self::validate($rule,$data,$msg);

			$res = User::edit($data);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}


	/*
 	* @Description  删除数据
 	* @param (输入参数：)  {array}        where 删除条件
 	* @return (返回参数：) {bool}        
 	*/
	public static function delete($where){
		try{
			$res = User::delete($where);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}




}

