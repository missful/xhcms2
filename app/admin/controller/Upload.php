<?php

//文件上传类

namespace app\admin\controller;
use think\facade\Log;
use think\facade\Validate;
use think\facade\Filesystem;

class Upload extends Admin{
	
	
	private function upload($filekey){
		$file = $this->request->file($filekey);
		try{
			if(!config('xhadmin.file_size')){
				$file_sieze = 50 * 1024 * 1024;
			}else{
				$file_sieze = config('xhadmin.file_size') * 1024 * 1024;
			}
			
			if(!config('xhadmin.file_type')){
				$file_type = 'gif,png,jpg,jpeg,doc,docx,csv,pdf,rar,zip,txt,mp4,flv';
			}else{
				$file_type = config('xhadmin.file_type');
			}
			
			if(!Validate::fileExt($file,$file_type) || !Validate::fileSize($file,$file_sieze)){
				throw new \Exception('上传验证失败');
			} 

			if(config('my.oss_status')){
				$url = \utils\oss\OssService::OssUpload(['tmp_name'=>$file->getPathname(),'extension'=>$file->extension()]);
			}else{
				$info = Filesystem::disk('public')->putFile(\utils\oss\OssService::setFilepath(),$file,'uniqid');
				$url = \utils\oss\OssService::getAdminFileName(basename($info));
			}
		}catch(\Exception $e){
			log::error('图片上传错误：'.print_r($e->getMessage(),true));
			throw new \Exception($e->getMessage());
		}
		return $url;
	}
	
	
	//普通图片上传
	public function uploadImages()
	{
		$url = $this->upload('file');
		if($url){
			return json(['code'=>1,'data'=>$url]);
		}else{
			return json(['code'=>0,'msg'=>'上传失败']);
		}
	}
	
	
	//xheditor编辑器上传
	public function editorUpload() {
		$url = $this->upload('filedata');
		if($url){
			echo '{err: "", msg: {url: "!'.$url.'", localname: "", id: "1"}}';
		}
	}
	
	
	//百度编辑器上传
	public function uploadUeditor(){
		$ueditor_config = json_decode(preg_replace("/\/\*[\s\S]+?\*\//", "", file_get_contents("static/js/ueditor/php/config.json")), true);
        $action = $_GET['action'];
        switch ($action) {
            case 'config':
                $result = json_encode($ueditor_config);
                break;
            /* 上传图片 */
            case 'uploadimage':
                /* 上传涂鸦 */
            case 'uploadscrawl':
                /* 上传视频 */
            case 'uploadvideo':
                /* 上传文件 */
            case 'uploadfile':
				$url = $this->upload('upfile');
				$result = json_encode(array(
					'url' => $url,
					'title' => htmlspecialchars($_POST['pictitle'], ENT_QUOTES),
					'original' => basename($url),
					'state' => 'SUCCESS'
				));
                break;
            default:
                $result = json_encode(array(
                    'state' => '请求地址出错'
                ));
                break;
        }
        /* 输出结果 */
        if (isset($_GET["callback"])) {
            if (preg_match("/^[\w_]+$/", $_GET["callback"])) {
                echo htmlspecialchars($_GET["callback"]) . '(' . $result . ')';
            } else {
                echo json_encode(array(
                    'state' => 'callback参数不合法'
                ));
            }
        } else {
            echo $result;
        }
	}
	
	
    
}